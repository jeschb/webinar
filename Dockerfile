# Build a small nginx image with static website
FROM nginx:1.17.8-alpine
RUN rm -rf /usr/share/nginx/html/*
COPY /dist/webinar/ /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
